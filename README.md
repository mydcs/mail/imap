# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/mail/imap/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/mail/imap/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/mail/imap/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/mail/imap/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/mail/imap?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/mail/imap

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/imap.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/imap
[docker-release-image]: https://img.shields.io/docker/v/mydcs/imap?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/imap
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/imap.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/imap
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/imap/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/imap


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

## Vorbereitung

1. Arbeitsverzeichnis anlegen
2. In das Arbeitsverzeichnis wechseln
3. Unterordner config anlegen
4. Container starten, entweder ueber "docker run" oder "docker compose"

## Aufruf

### CLI

```
docker run -it --rm -e DOMAIN=example -e TLD=com -v $(pwd)/config:/srv -v /etc/letsencrypt:/etc/letsencrypt mydcs/imap:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  imap:
    container_name: imap
    hostname: imap
    restart: always
    image: mydcs/imap:latest
    environment:
      - DOMAIN=${DOMAIN:-example}
      - TLD=${TLD:-com}
    networks:
      - dcs
    ports:
      - 24:24
      - 993:993
    volumes:
      - $PWD/config:/srv
      - /etc/letsencrypt:/etc/letsencrypt

networks:
  dcs:
    name: dcs
```

