FROM	alpine:3.17
LABEL	maintainer=Dis4sterRec0very

#
# Setup environment
#
ENV	DOMAIN=${DOMAIN:-example}
ENV	TLD=${TLD:-com}
ENV	ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}

#
# Install Dovecot
#
RUN apk --no-cache add \
	dovecot \
	dovecot-ldap \
	dovecot-fts-xapian \
	dovecot-lmtpd \
	dovecot-pigeonhole-plugin \
	dovecot-pop3d \
	dovecot-submissiond \
	rspamd-client \
	xapian-core \
	openldap-clients

# Activate LDAP authentication in base conf
RUN sed "s/^#!include auth-ldap.conf.ext/!include auth-ldap.conf.ext/g" /etc/dovecot/conf.d/10-auth.conf

# files to image the easy way
COPY img_fs /

RUN ln -s /etc/dovecot/dovecot-ldap.conf.ext /etc/dovecot/dovecot-ldap-passdb.conf.ext
RUN ln -s /etc/dovecot/dovecot-ldap.conf.ext /etc/dovecot/dovecot-ldap-userdb.conf.ext
RUN chmod 777 /srv

HEALTHCHECK --start-period=350s CMD echo QUIT|nc localhost 24|grep "Dovecot ready."

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 24/tcp 110/tcp 143/tcp 993/tcp 4190/tcp 2525/tcp
