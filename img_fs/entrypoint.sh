#!/bin/sh

DOMAIN=${DOMAIN:-example}
TLD=${TLD:-com}
ADMIN_PASSWORD=${ADMIN_PASSWORD:-secret}

sed -i "s#dc=example,dc=com#dc=$DOMAIN,dc=$TLD#g" /etc/dovecot/dovecot-ldap.conf.ext
sed -i "s#^dnpass.*#dnpass = $ADMIN_PASSWORD#g" /etc/dovecot/dovecot-ldap.conf.ext

dovecot -F -c /etc/dovecot/dovecot.conf