#!/bin/bash

export NAME=$(basename $PWD)
export TAG=latest

docker build -t $NAME:$TAG .

docker run -it --rm \
    $* \
    -p 993:993 \
    -p 24:24 \
    --name $NAME \
    --network dcs \
    --hostname $NAME \
    $NAME:$TAG

#docker image prune -a -f
